package com.example.dto;

import java.io.Serializable;

public class UserDto implements Serializable {

    private Long id;

    private String username;

    private String address;

    private String phone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    public UserDto() {
    }

    public UserDto(Long id, String username, String address, String phone) {
        this.id = id;
        this.username = username;
        this.address = address;
        this.phone = phone;
    }
}
